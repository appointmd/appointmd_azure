﻿using Consulting.Core.Entities;
using Consulting.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Core.Repository
{
    public interface IConsultingRepository
    {
        Task<Pagination<Consultation>> getConsultations(ConsultingSpecificationParameters consultingSpecificationParameters);

        Task<IEnumerable<Consultation>> getConsultationByName(string name);

        Task<Consultation> getConsultationById(string id);

        Task<Consultation> CreateConsultation(Consultation consultation);
        Task<bool> UpdateConsultation(Consultation consultation);
        Task<bool> DeleteConsultation(string id);
    }
}
