﻿using Consulting.Core.Entities;
using Consulting.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Core.Repositories
{
    public interface IAttendeeRepository
    {
        Task<IEnumerable<User>> getAttendees();

        Task<IEnumerable<User>> getAttendeeByName(string name);

        Task<User> getAttendeeById(string id);

    }
}
