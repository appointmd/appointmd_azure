﻿using AutoMapper;
using Consulting.Application.Commands;
using Consulting.Application.Responses;
using Consulting.Core.Entities;
using Consulting.Core.Specifications;
using EventBus.Messages.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Application.Mappers
{
    public class ConsultationMappingProfile : Profile
    {
        public ConsultationMappingProfile() 
        {
            CreateMap<Consultation, ConsultationResponse>().ReverseMap();
            CreateMap<Consultation, CreateConsultationCommand>().ReverseMap();
            CreateMap<RealizeAppointmentCommand, Consultation>().ReverseMap();
            CreateMap<RealizeAppointmentEvent, RealizeAppointmentCommand>().ReverseMap();
            CreateMap<User, AttendeeResponse>().ReverseMap();
            CreateMap<Pagination<Consultation>, Pagination<ConsultationResponse>>().ReverseMap();
        }
    }
}
