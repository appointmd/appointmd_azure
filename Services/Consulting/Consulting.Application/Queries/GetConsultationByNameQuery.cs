﻿using Amazon.Runtime.Internal;
using Consulting.Application.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace Consulting.Application.Queries
{
    public class GetConsultationByNameQuery : IRequest<IList<ConsultationResponse>>
    {
        public string Name { get; set; }

        public GetConsultationByNameQuery(string name)
        {
            Name = name;
        }
    }
}
