﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consulting.Application.Responses;
using MediatR;

namespace Consulting.Application.Queries
{
    public class GetConsultationByIdQuery : IRequest<ConsultationResponse>
    {
        public string Id { get; set; }

        public GetConsultationByIdQuery(string id)
        {
            Console.WriteLine("inside query " + id);
            Id = id;
        }
    }
}
