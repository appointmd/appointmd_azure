﻿using Consulting.Application.Responses;
using Consulting.Core.Specifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace Consulting.Application.Queries
{
    public class GetAllConsultationsQuery : IRequest<Pagination<ConsultationResponse>>
    {
        public ConsultingSpecificationParameters SpecificationParameters { get; set; }

        public GetAllConsultationsQuery(ConsultingSpecificationParameters consultingSpecificationParameters)
        {
            SpecificationParameters = consultingSpecificationParameters;
        }
    }
}
