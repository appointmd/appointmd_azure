﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consulting.Core.Entities;
using MediatR;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using Consulting.Application.Responses;

namespace Consulting.Application.Commands
{
    public class RealizeAppointmentCommand : IRequest<string>
    {
    
        [BsonElement("Name")]
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Status { get; set; }
        public string? Location { get; set; }
        // people
        public List<User>? Attendees { get; set; }

        public string? AppointmentId { get; set; }

        public string? OrganizationId { get; set; }

        public DateTime? PlannedForDate { get; set; }
    }
}
