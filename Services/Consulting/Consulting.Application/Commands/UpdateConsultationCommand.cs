﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Consulting.Core.Entities;

namespace Consulting.Application.Commands
{
    public class UpdateConsultationCommand : IRequest<bool>
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        public string OrganizationId { get; set; }
        public string Location { get; set; }
        public List<User> Attendees { get; set; }
        public string AppointmentId { get; set; }
    }
}
