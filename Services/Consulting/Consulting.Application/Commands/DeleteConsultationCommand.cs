﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Application.Commands
{
    public class DeleteConsultationCommand : IRequest<bool>
    {
        public string Id { get; set; }

        public DeleteConsultationCommand(string id)
        {
            Id = id;
        }
    }
}
