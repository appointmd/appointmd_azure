﻿using Amazon.Runtime.Internal;
using Consulting.Application.Responses;
using Consulting.Core.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace Consulting.Application.Commands
{
    public class CreateConsultationCommand : IRequest<ConsultationResponse>
    {

        [BsonElement("Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
        public List<User> Attendees { get; set; }
        public string AppointmentId { get; set; }

        public string OrganizationId { get; set; }
    }
}
