﻿using Consulting.Application.Commands;
using Consulting.Core.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Application.Handlers
{
    public class DeleteConsultationCommandHandler : IRequestHandler<DeleteConsultationCommand, bool>
    {
        private readonly IConsultingRepository _consultingRepository;

        public DeleteConsultationCommandHandler(IConsultingRepository consultingRepository)
        {
            _consultingRepository = consultingRepository;
        }

        public async Task<bool> Handle(DeleteConsultationCommand request, CancellationToken cancellationToken)
        {
            return await _consultingRepository.DeleteConsultation(request.Id);
        }
    }
}
