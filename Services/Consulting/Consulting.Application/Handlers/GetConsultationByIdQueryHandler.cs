﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consulting.Application.Mappers;
using Consulting.Application.Queries;
using Consulting.Application.Responses;
using Consulting.Core.Repository;
using MediatR;

namespace Consulting.Application.Handlers
{
    public class GetConsultationByIdQueryHandler : IRequestHandler<GetConsultationByIdQuery, ConsultationResponse>
    {
        private readonly IConsultingRepository _consultatingRepository;

        public GetConsultationByIdQueryHandler(IConsultingRepository consultingRepository)
        {
            _consultatingRepository = consultingRepository;
        }

        public async Task<ConsultationResponse> Handle(GetConsultationByIdQuery request, CancellationToken cancellationToken)
        {
            var consultation = await _consultatingRepository.getConsultationById(request.Id);
            var consultationResponse = ConsultingMapper.Mapper.Map<ConsultationResponse>(consultation);
            return consultationResponse;
        }
    }
}
