﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Runtime.Internal.Util;
using AutoMapper;
using Consulting.Application.Commands;
using Consulting.Application.Mappers;
using Consulting.Core.Entities;
using Consulting.Core.Repository;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Consulting.Application.Handlers
{
    public class RealizeAppointmentCommandHandler : IRequestHandler<RealizeAppointmentCommand, string>
    {

        private readonly IConsultingRepository _consultingRepository;
        private readonly ILogger<RealizeAppointmentCommandHandler> _logger;

        public RealizeAppointmentCommandHandler(IConsultingRepository consultingRepository, ILogger<RealizeAppointmentCommandHandler> logger)
        {
            _consultingRepository = consultingRepository;
            _logger = logger;
        }

        public async Task<string> Handle(RealizeAppointmentCommand request, CancellationToken cancellationToken)
        {
            var consultationEntity = ConsultingMapper.Mapper.Map<Consultation>(request);

            if (consultationEntity is null)
            {
                throw new ApplicationException("There is an issue with mapping while realizing your consultation.");
            }
            var generatedConsultation = await _consultingRepository.CreateConsultation(consultationEntity);
            _logger.LogInformation($"Consultation {generatedConsultation} succesfully created.");
            return generatedConsultation.Id;
        }
    }
}
