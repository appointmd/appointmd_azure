﻿using Amazon.Runtime.Internal;
using Consulting.Application.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Consulting.Core.Repository;
using Consulting.Core.Entities;

namespace Consulting.Application.Handlers
{
    public class UpdateConsultationCommandHandler : IRequestHandler<UpdateConsultationCommand, bool>
    {
        private readonly IConsultingRepository _consultationRepository;

        public UpdateConsultationCommandHandler(IConsultingRepository consultingRepository)
        {
            _consultationRepository = consultingRepository;
        }

        public async Task<bool> Handle(UpdateConsultationCommand request, CancellationToken cancellationToken)
        {
            var consultationEntity = await _consultationRepository.UpdateConsultation(new Consultation
            {
                Id = request.Id,
                Status = request.Status,
                Location = request.Location,
                AppointmentId = request.AppointmentId,
                Name = request.Name,
                Attendees = request.Attendees,
                Description = request.Description,
                OrganizationId = request.OrganizationId
            });

            return consultationEntity;
        }
    }
}
