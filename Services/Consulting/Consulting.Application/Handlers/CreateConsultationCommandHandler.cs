﻿using Consulting.Application.Commands;
using Consulting.Application.Mappers;
using Consulting.Application.Responses;
using Consulting.Core.Entities;
using Consulting.Core.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Application.Handlers
{
    public class CreateConsultationCommandHandler : IRequestHandler<CreateConsultationCommand, ConsultationResponse>
    {
        private readonly IConsultingRepository _consultingRepository;

        public CreateConsultationCommandHandler(IConsultingRepository consultingRepository)
        {
            _consultingRepository = consultingRepository;
        }

        public async Task<ConsultationResponse> Handle(CreateConsultationCommand request, CancellationToken cancellationToken)
        {
            var consultationEntity = ConsultingMapper.Mapper.Map<Consultation>(request);
            if(consultationEntity is null)
            {
                throw new ApplicationException("There is an issue with mapping while creating a new consultation.");
            }

            var newConsultation = await _consultingRepository.CreateConsultation(consultationEntity);
            var consultationResponse = ConsultingMapper.Mapper.Map<ConsultationResponse>(newConsultation);
            return consultationResponse;
        }
    }
}
