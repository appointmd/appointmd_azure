﻿using Consulting.Application.Mappers;
using Consulting.Application.Queries;
using Consulting.Application.Responses;
using Consulting.Core.Repository;
using Consulting.Core.Specifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Application.Handlers
{
    public class GetAllConsultationsQueryHandler : IRequestHandler<GetAllConsultationsQuery, Pagination<ConsultationResponse>>
    {
        private readonly IConsultingRepository _consultingRepository;

        public GetAllConsultationsQueryHandler(IConsultingRepository consultingRepository)
        {
            _consultingRepository = consultingRepository;
        }

        public async Task<Pagination<ConsultationResponse>> Handle(GetAllConsultationsQuery request, CancellationToken cancellationToken)
        {
            var consultation = await _consultingRepository.getConsultations(request.SpecificationParameters);
            var consultationResponse = ConsultingMapper.Mapper.Map<Pagination<ConsultationResponse>>(consultation);
            return consultationResponse;
        }
    }
}
