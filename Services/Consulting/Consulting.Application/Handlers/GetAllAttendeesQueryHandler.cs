﻿using Consulting.Application.Mappers;
using Consulting.Application.Queries;
using Consulting.Application.Responses;
using Consulting.Core.Entities;
using Consulting.Core.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Application.Handlers
{
    public class GetAllAttendeesQueryHandler : IRequestHandler<GetAllAttendeesQuery,IList<AttendeeResponse>>
    {
        private readonly IAttendeeRepository _attendeeRepository;

        public GetAllAttendeesQueryHandler(IAttendeeRepository attendeeRepository)
        {
            _attendeeRepository = attendeeRepository;
        }

        public async Task<IList<AttendeeResponse>> Handle(GetAllAttendeesQuery request, CancellationToken cancellationToken)
        {
            var attendees = await _attendeeRepository.getAttendees();
            var attendeesResponse = ConsultingMapper.Mapper.Map<IList<User>, IList<AttendeeResponse>>(attendees.ToList());
            return attendeesResponse;
        }
    }
}
