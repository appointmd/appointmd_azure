﻿using Amazon.Runtime.Internal;
using Consulting.Application.Mappers;
using Consulting.Application.Queries;
using Consulting.Application.Responses;
using Consulting.Core.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Application.Handlers
{
    public class GetConsultationByNameHandler : IRequestHandler<GetConsultationByNameQuery, IList<ConsultationResponse>>
    {
        private readonly IConsultingRepository _consultatingRepository;

        public GetConsultationByNameHandler(IConsultingRepository consultatingRepository)
        {
            _consultatingRepository = consultatingRepository;
        }

        public async Task<IList<ConsultationResponse>> Handle(GetConsultationByNameQuery request, CancellationToken cancellationToken)
        {
            var consultation = await _consultatingRepository.getConsultationByName(request.Name);
            var consultationResponse = ConsultingMapper.Mapper.Map<IList<ConsultationResponse>>(consultation);
            return consultationResponse;
        }
    }
}
