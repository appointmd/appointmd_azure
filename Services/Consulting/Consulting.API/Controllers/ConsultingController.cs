﻿using Consulting.Application.Commands;
using Consulting.Application.Queries;
using Consulting.Application.Responses;
using Consulting.Core.Specifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Consulting.API.Controllers
{
    public class ConsultingController : ApiController
    {
        private readonly IMediator _mediator;

        public ConsultingController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetAllConsultations")]
        [ProducesResponseType(typeof(IList<ConsultationResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IList<ConsultationResponse>>> GetAllConsultations([FromQuery] ConsultingSpecificationParameters consultingSpecificationParameters)
        {
            var query = new GetAllConsultationsQuery(consultingSpecificationParameters);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("[action]/{id}", Name = "GetConsultationById")]
        [ProducesResponseType(typeof(ConsultationResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ConsultationResponse>> GetConsultationById(string id)
        {
            var query = new GetConsultationByIdQuery(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("[action]/{consultationName}", Name = "GetConsultationByName")]
        [ProducesResponseType(typeof(ConsultationResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ConsultationResponse>> GetConsultationByName(string consultationName)
        {
            var query = new GetConsultationByNameQuery(consultationName);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllAttendees")]
        [ProducesResponseType(typeof(AttendeeResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<AttendeeResponse>> GetAllAttendees()
        {
            var query = new GetAllAttendeesQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }
        // POST
        [HttpPost]
        [Route("CreateConsultation")]
        [ProducesResponseType(typeof(ConsultationResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ConsultationResponse>> CreateConsultation([FromBody] CreateConsultationCommand createConsultationCommand)
        {
            var result = await _mediator.Send(createConsultationCommand);
            return Ok(result);
        }
        // for testing purposes
        [HttpPost]
        [Route("RealizeAppointment")]
         public async Task<ActionResult<ConsultationResponse>> RealizeAppointment([FromBody] RealizeAppointmentCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }
        // UPDATE
        [HttpPut]
        [Route("UpdateConsultation")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateConsultation([FromBody] UpdateConsultationCommand updateConsultationCommand)
        {
            var result = await _mediator.Send(updateConsultationCommand);
            return Ok(result);
        }
        // DELETE
        [HttpDelete]
        [Route("{id}", Name = "DeleteConsultation")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteConsultation(string id)
        {
            var command = new DeleteConsultationCommand(id);
            var result = await _mediator.Send(command); ;
            return Ok(result);
        }

    }
}
