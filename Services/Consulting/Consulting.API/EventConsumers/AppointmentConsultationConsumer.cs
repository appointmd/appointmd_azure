﻿using AutoMapper;
using EventBus.Messages.Events;
using MassTransit;
using MediatR;
using Consulting.Application.Commands;
using Consulting.Application.Mappers;

namespace Consulting.API.EventConsumers
{
    public class AppointmentConsultationConsumer : IConsumer<RealizeAppointmentEvent>
    {
        private readonly IMediator _mediator;
    
        private readonly ILogger<AppointmentConsultationConsumer> _logger;
        public AppointmentConsultationConsumer(IMediator mediator, ILogger<AppointmentConsultationConsumer> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<RealizeAppointmentEvent> context)
        {
            var command = ConsultingMapper.Mapper.Map<RealizeAppointmentCommand>(context.Message);
            var result = await _mediator.Send(command);
            _logger.LogInformation("Appointment realized into consultation.");
        }
    }
}
