﻿using Consulting.API.EventConsumers;
using Consulting.Application.Handlers;
using Consulting.Application.Responses;
using Consulting.Core.Repositories;
using Consulting.Core.Repository;
using Consulting.Infrastructure.Data;
using Consulting.Infrastructure.Repository;
using EventBus.Messages.Common;
using HealthChecks.UI.Client;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace Consulting.API
{
    public class Startup
    {
        public IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
        
            services.AddControllers();
            services.AddApiVersioning();
            // DI
            services.AddAutoMapper(typeof(Startup));
            services.AddMediatR(typeof(CreateConsultationCommandHandler).GetTypeInfo().Assembly);
            services.AddScoped<IConsultingContext, ConsultingContext>();
            services.AddScoped<IConsultingRepository, ConsultingRepository>();
            services.AddScoped<IAttendeeRepository, ConsultingRepository>();
            services.AddScoped<AppointmentConsultationConsumer>();
            services.AddHealthChecks().AddMongoDb(Configuration["DatabaseSettings:ConnectionString"], "Scheduling Mongodb HealthCheck", HealthStatus.Degraded);
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo { Title = "Consulting.API", Version = "v1" }); });
            services.AddMassTransit(config =>
            {
                // consumer
                config.AddConsumer<AppointmentConsultationConsumer>();
                config.UsingRabbitMq((ct, cfg) =>
                {
                    cfg.Host(Configuration["EventBusSettings:HostAdress"]);
                    // queue name
                    cfg.ReceiveEndpoint(EventBusConstants.AppointmentToConsultationQueue, c =>
                    {
                        c.ConfigureConsumer<AppointmentConsultationConsumer>(ct);
                    });
                });
            });
            services.AddMassTransitHostedService();
            // Auth
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://dev-qq6v6kua7p6ps2q7.us.auth0.com/";
                options.Audience = "Consulting API";
            });

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
          
            if (env.IsDevelopment())
            {
              
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Consulting.API v1"));
            }
    
            app.UseRouting();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
            });
        }

    }
}
