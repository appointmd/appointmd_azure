﻿using Consulting.Core.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Consulting.Infrastructure.Data
{
    public static class AttendeesContextSeeder
    {

        public static void SeedData(IMongoCollection<User> AttendeesCollection) 
        {
            bool checkAttendees = AttendeesCollection.Find(a => true).Any();
            string path = Path.Combine("Data", "SeedData", "attendees.json");

            if(!checkAttendees)
            {
                var attendeesData = File.ReadAllText(path);
                var attendees = JsonSerializer.Deserialize<List<User>>(attendeesData);
                if(attendees != null)
                {
                    foreach(var attendee in attendees)
                    {
                        AttendeesCollection.InsertOneAsync(attendee);
                    }
                }
            }
        }
    }
}
