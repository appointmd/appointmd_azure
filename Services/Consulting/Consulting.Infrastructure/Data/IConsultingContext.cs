﻿using Consulting.Core.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Infrastructure.Data
{
    public interface IConsultingContext
    {
        IMongoCollection<Consultation> Consultations { get; }

        IMongoCollection<User> Attendees { get; }
    }
}
