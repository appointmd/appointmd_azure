﻿using System.Text.Json;
using Consulting.Core.Entities;
using MongoDB.Driver;

namespace Consulting.Infrastructure.Data
{
    public static class ConsultationContextSeeder
    {
        public static void SeedData(IMongoCollection<Consultation> ConsultationCollection)
        {
            bool checkConsultations = ConsultationCollection.Find(c => true).Any();
            string path = Path.Combine("Data", "SeedData", "consultations.json");

           
            if (!checkConsultations)
            {
                var consultationData = File.ReadAllText(path);
                var consultations = JsonSerializer.Deserialize<List<Consultation>>(consultationData);
                if(consultations != null)
                {
                    foreach (var consultation in consultations)
                    {
                        ConsultationCollection.InsertOneAsync(consultation);
                    }
                }
                
            }
        }
    }
}
