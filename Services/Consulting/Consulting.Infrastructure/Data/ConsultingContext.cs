﻿using Consulting.Core.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Infrastructure.Data
{
    public class ConsultingContext : IConsultingContext
    {
        public IMongoCollection<Consultation> Consultations { get; }

        public IMongoCollection<User> Attendees { get; }

        public ConsultingContext(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
            var database = client.GetDatabase(configuration.GetValue<string>("DatabaseSettings:DatabaseName"));

            Consultations = database.GetCollection<Consultation>(configuration.GetValue<string>("DatabaseSettings:ConsultationsCollection"));
            Attendees = database.GetCollection<User>(configuration.GetValue<string>("DatabaseSettings:AttendeesCollection"));
            
            ConsultationContextSeeder.SeedData(Consultations);
            AttendeesContextSeeder.SeedData(Attendees);
          

            Console.WriteLine(Consultations.EstimatedDocumentCount());
        }
    }
}
