﻿using Consulting.Core.Entities;
using Consulting.Core.Repositories;
using Consulting.Core.Repository;
using Consulting.Core.Specifications;
using Consulting.Infrastructure.Data;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consulting.Infrastructure.Repository
{
    public class ConsultingRepository : IConsultingRepository, IAttendeeRepository
    {
        private readonly IConsultingContext _context;

        public ConsultingRepository(IConsultingContext consultingContext)
        {
            _context = consultingContext;
        }
     /*   public async Task<User> CreateAttendee(User attendee)
        {
            await _context.Attendees.InsertOneAsync(attendee);
            return attendee;
        }*/

        public async Task<Consultation> CreateConsultation(Consultation consultation)
        {
            await _context.Consultations.InsertOneAsync(consultation);
            return consultation;
        }

      /*  public async Task<bool> DeleteAttendee(string id)
        {
            FilterDefinition<User> filter = Builders<User>.Filter.Eq(a => a.Id, id);
            DeleteResult deleteResult = await _context
                .Attendees
                .DeleteOneAsync(filter);

            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }*/

        public async Task<bool> DeleteConsultation(string id)
        {
            FilterDefinition<Consultation> filter = Builders<Consultation>.Filter.Eq(c => c.Id, id);
            DeleteResult deleteResult = await _context
                .Consultations
                .DeleteOneAsync(filter);

            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }

        public async Task<User> getAttendeeById(string id)
        {
            return await _context.Attendees.Find(a => a.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<User>> getAttendeeByName(string name)
        {
            FilterDefinition<User> filter = Builders<User>.Filter.Eq(a => a.Name, name);
            return await _context
                .Attendees
                .Find(filter)
                .ToListAsync();
        }

        public async Task<IEnumerable<User>> getAttendees()
        {
            return await _context
               .Attendees
               .Find(u => true)
               .ToListAsync();
        }

        public async Task<Consultation> getConsultationById(string id)
        {
            Console.WriteLine("repo get id " + id);
            return await _context.Consultations.Find(c => c.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Consultation>> getConsultationByName(string name)
        {
            FilterDefinition<Consultation> filter = Builders<Consultation>.Filter.Eq(c => c.Name, name);
            return await _context
                .Consultations
                .Find(filter)
                .ToListAsync();
        }

        public async Task<Pagination<Consultation>> getConsultations(ConsultingSpecificationParameters consultingSpecificationParameters)
        {
            var builder = Builders<Consultation>.Filter;
            var filter = builder.Empty;
            if (!string.IsNullOrEmpty(consultingSpecificationParameters.Search))
            {
                var searchFilter = builder.Regex(x => x.Name, new MongoDB.Bson.BsonRegularExpression(consultingSpecificationParameters.Search));
                filter &= searchFilter;
            }

            if (!string.IsNullOrEmpty(consultingSpecificationParameters.Sort))
            {
                return new Pagination<Consultation>
                {
                    PageSize = consultingSpecificationParameters.PageSize,
                    PageIndex = consultingSpecificationParameters.PageIndex,
                    Data = await _context.Consultations.Find(filter)
                    .Sort(Builders<Consultation>.Sort.Ascending("Name"))
                    .Skip(consultingSpecificationParameters.PageIndex - 1)
                    .Limit(consultingSpecificationParameters.PageSize)
                    .ToListAsync(),
                    Count = await _context.Consultations.CountDocumentsAsync(p => true) // apply UI
                };
            }

            return new Pagination<Consultation>()
            {
                PageSize = consultingSpecificationParameters.PageSize,
                PageIndex = consultingSpecificationParameters.PageIndex,
                Data = await _context.Consultations
                   .Find(filter)
                   .Sort(Builders<Consultation>.Sort.Ascending("Name"))
                   .Skip(consultingSpecificationParameters.PageSize * (consultingSpecificationParameters.PageIndex - 1))
                   .Limit(consultingSpecificationParameters.PageSize).ToListAsync(),
                Count = await _context.Consultations.CountDocumentsAsync(p => true)
            };
        }


        // update attendees implemented in future
      /*  public async Task<bool> UpdateAttendee(string attendee)
        {
            throw new NotImplementedException();
        }*/

        public async Task<bool> UpdateConsultation(Consultation consultation)
        {
            var updateResult = await _context
              .Consultations
              .ReplaceOneAsync(c => c.Id == consultation.Id, consultation);

            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }
    }
}
