﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace MedicalRecords.Application.Commands
{
    public class UpdateMedicalRecordCommand : IRequest<bool>
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string PatientId { get; set; }
        public string AuthorId { get; set; }
        public string OrganizationId { get; set; }
        public string SessionId { get; set; }
        public string SessionLocation { get; set; }
        public string ConsultationId { get; set; }
    }
}
