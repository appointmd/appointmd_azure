﻿using AutoMapper;
using MedicalRecords.Application.Commands;
using MedicalRecords.Application.Responses;
using MedicalRecords.Core.Entities;
using MedicalRecords.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Application.Mappers
{
    public class MedicalRecordMappingProfile : Profile
    {
        public MedicalRecordMappingProfile()
        {
            CreateMap<MedicalRecord, MedicalRecordResponse>().ReverseMap();
            CreateMap<MedicalRecord, CreateMedicalRecordCommand>().ReverseMap();
            CreateMap<Pagination<MedicalRecord>, Pagination<MedicalRecordResponse>>().ReverseMap();
        }
    }
}
