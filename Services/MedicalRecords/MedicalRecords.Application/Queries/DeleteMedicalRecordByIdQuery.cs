﻿using Amazon.Runtime.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace MedicalRecords.Application.Queries
{
    public class DeleteMedicalRecordByIdQuery : IRequest<bool>
    {

        public string Id { get; set; }

        public DeleteMedicalRecordByIdQuery(string id)
        {
            Id = id;
        }
    }
}
