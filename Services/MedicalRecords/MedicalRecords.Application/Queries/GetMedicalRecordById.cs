﻿using Amazon.Runtime.Internal;
using MedicalRecords.Application.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace MedicalRecords.Application.Queries
{
    public class GetMedicalRecordById :IRequest<MedicalRecordResponse>
    {

        public string Id { get; set; }

        public GetMedicalRecordById(string id)
        {
            Id = id;   
        }
    }
}
