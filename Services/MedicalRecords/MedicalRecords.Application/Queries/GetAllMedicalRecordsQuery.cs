﻿using Amazon.Runtime.Internal;
using MedicalRecords.Application.Responses;
using MedicalRecords.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR; 

namespace MedicalRecords.Application.Queries
{
    public class GetAllMedicalRecordsQuery : IRequest<Pagination<MedicalRecordResponse>>
    {
        public MedicalRecordSpecificationParameters MedicalRecordSpecificationParameters { get; set; }
        public GetAllMedicalRecordsQuery(MedicalRecordSpecificationParameters medicalRecordSpecificationParameters)
        {

            MedicalRecordSpecificationParameters = medicalRecordSpecificationParameters;

        }   
    }
}
