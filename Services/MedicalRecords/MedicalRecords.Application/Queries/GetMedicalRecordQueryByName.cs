﻿using MediatR;
using MedicalRecords.Application.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Application.Queries
{
    public class GetMedicalRecordQueryByName : IRequest<IList<MedicalRecordResponse>>
    {
        public string Name { get; set; }

        public GetMedicalRecordQueryByName(string name)
        {
            Name = name;
        }
    }
}
