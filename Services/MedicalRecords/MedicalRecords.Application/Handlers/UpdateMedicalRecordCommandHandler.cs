﻿using MediatR;
using MedicalRecords.Application.Commands;
using MedicalRecords.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Application.Handlers
{
    public class UpdateMedicalRecordCommandHandler : IRequestHandler<UpdateMedicalRecordCommand, bool>
    {
        private readonly IMedicalRecordRepository _medicalRecordRepository;

        public UpdateMedicalRecordCommandHandler(IMedicalRecordRepository medicalRecordRepository)
        {
            _medicalRecordRepository = medicalRecordRepository;
        }

        public async Task<bool> Handle(UpdateMedicalRecordCommand request, CancellationToken cancellationToken)
        {
            var medicalRecordEntity = await _medicalRecordRepository.UpdateMedicalRecord(new Core.Entities.MedicalRecord
            {
                Id = request.Id,
                Name = request.Name,
                PatientId = request.PatientId,
                Type = request.Type,
                AuthorId = request.AuthorId,
                OrganizationId = request.OrganizationId,
                SessionId = request.SessionId,
                SessionLocation = request.SessionLocation,
                ConsultationId = request.ConsultationId
            });

            return medicalRecordEntity;
        }
    }
}
