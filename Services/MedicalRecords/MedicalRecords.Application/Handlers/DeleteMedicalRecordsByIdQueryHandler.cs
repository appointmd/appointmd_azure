﻿using MediatR;
using MedicalRecords.Application.Queries;
using MedicalRecords.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Application.Handlers
{
    public class DeleteMedicalRecordsByIdQueryHandler : IRequestHandler<DeleteMedicalRecordByIdQuery, bool>
    {
        private readonly IMedicalRecordRepository _medicalRecordRepository;

        public DeleteMedicalRecordsByIdQueryHandler(IMedicalRecordRepository medicalRecordRepository)
        {
            _medicalRecordRepository = medicalRecordRepository;
        }

        public async Task<bool> Handle(DeleteMedicalRecordByIdQuery request, CancellationToken cancellationToken)
        {
            return await _medicalRecordRepository.DeleteMedicalRecord(request.Id);
        }
    }
}
