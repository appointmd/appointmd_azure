﻿using MediatR;
using MedicalRecords.Application.Mappers;
using MedicalRecords.Application.Queries;
using MedicalRecords.Application.Responses;
using MedicalRecords.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Application.Handlers
{
    public class GetMedicalRecordByNameQueryHandler : IRequestHandler<GetMedicalRecordQueryByName, IList<MedicalRecordResponse>>
    {
        private readonly IMedicalRecordRepository _medicalRecordRepository;

        public GetMedicalRecordByNameQueryHandler(IMedicalRecordRepository medicalRecordRepository)
        {
            _medicalRecordRepository = medicalRecordRepository;
        }

        public async Task<IList<MedicalRecordResponse>> Handle(GetMedicalRecordQueryByName request, CancellationToken cancellationToken)
        {
            var medicalRecord = await _medicalRecordRepository.GetMedicalRecordsByName(request.Name);
            var medicalRecordResponse = MedicalRecordMapper.Mapper.Map<IList<MedicalRecordResponse>>(medicalRecord);
            return medicalRecordResponse;
        }
    }
}
