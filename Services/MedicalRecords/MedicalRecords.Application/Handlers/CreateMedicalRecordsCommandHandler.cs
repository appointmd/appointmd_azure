﻿using MediatR;
using MedicalRecords.Application.Commands;
using MedicalRecords.Application.Mappers;
using MedicalRecords.Application.Responses;
using MedicalRecords.Core.Entities;
using MedicalRecords.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Application.Handlers
{
    public class CreateMedicalRecordsCommandHandler : IRequestHandler<CreateMedicalRecordCommand, MedicalRecordResponse>
    {
        private readonly IMedicalRecordRepository _medicalRecordRepository;

        public CreateMedicalRecordsCommandHandler(IMedicalRecordRepository medicalRecordRepository)
        {
            _medicalRecordRepository = medicalRecordRepository;
        }

        public async Task<MedicalRecordResponse> Handle(CreateMedicalRecordCommand request, CancellationToken cancellationToken)
        {
            var medicalRecordEntity = MedicalRecordMapper.Mapper.Map<MedicalRecord>(request);
            if (medicalRecordEntity is null)
            {
                throw new ApplicationException("There is an issue with mapping while creating new medical record.");
            }

            var newMedicalRecord = await _medicalRecordRepository.CreateMedicalRecord(medicalRecordEntity);
            var medicalRecordResponse = MedicalRecordMapper.Mapper.Map<MedicalRecordResponse>(newMedicalRecord);
            return medicalRecordResponse;
        }
    }
}
