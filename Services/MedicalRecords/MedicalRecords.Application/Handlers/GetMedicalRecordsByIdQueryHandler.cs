﻿using MediatR;
using MedicalRecords.Application.Mappers;
using MedicalRecords.Application.Queries;
using MedicalRecords.Application.Responses;
using MedicalRecords.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Application.Handlers
{
    public class GetMedicalRecordsByIdQueryHandler : IRequestHandler<GetMedicalRecordById, MedicalRecordResponse>
    {
        private readonly IMedicalRecordRepository _medicalRecordRespistory;

        public GetMedicalRecordsByIdQueryHandler(IMedicalRecordRepository medicalRecordRespistory)
        {
            _medicalRecordRespistory = medicalRecordRespistory;
        }

        public async Task<MedicalRecordResponse> Handle(GetMedicalRecordById request, CancellationToken cancellationToken)
        {
            var medicalRecord = await _medicalRecordRespistory.GetMedicalRecordById(request.Id);
            var medicalRecordResponse = MedicalRecordMapper.Mapper.Map<MedicalRecordResponse>(medicalRecord);
            return medicalRecordResponse;
        }
    }
}
