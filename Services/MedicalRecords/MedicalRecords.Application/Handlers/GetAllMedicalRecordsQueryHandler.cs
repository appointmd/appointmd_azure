﻿using MediatR;
using MedicalRecords.Application.Mappers;
using MedicalRecords.Application.Queries;
using MedicalRecords.Application.Responses;
using MedicalRecords.Core.Repositories;
using MedicalRecords.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Application.Handlers
{
    public class GetAllMedicalRecordsQueryHandler : IRequestHandler<GetAllMedicalRecordsQuery, Pagination<MedicalRecordResponse>>
    {
        private readonly IMedicalRecordRepository _medicalRecordRepository;

        public GetAllMedicalRecordsQueryHandler(IMedicalRecordRepository medicalRecordRepository)
        {
            _medicalRecordRepository = medicalRecordRepository;
        }

        public async Task<Pagination<MedicalRecordResponse>> Handle(GetAllMedicalRecordsQuery request, CancellationToken cancellationToken)
        {
            var medicalRecord = await _medicalRecordRepository.GetMedicalRecords(request.MedicalRecordSpecificationParameters);
            var medicalRecordResponse = MedicalRecordMapper.Mapper.Map<Pagination<MedicalRecordResponse>>(medicalRecord);

            return medicalRecordResponse;
        }
    }
}
