﻿using MediatR;
using MedicalRecords.Application.Commands;
using MedicalRecords.Application.Queries;
using MedicalRecords.Application.Responses;
using MedicalRecords.Core.Specifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace MedicalRecords.API.Controllers
{
    public class MedicalRecordController : ApiController
    {

        private readonly IMediator _mediator;

        public MedicalRecordController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("[action]/{id}", Name = "GetMedicalRecordById")]
        [ProducesResponseType(typeof(MedicalRecordResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MedicalRecordResponse>> GetMedicalRecordById(string id)
        {
            var query = new GetMedicalRecordById(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("[action]/{medicalRecordName}", Name = "GetMedicalRecordName")]
        [ProducesResponseType(typeof(IList<MedicalRecordResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IList<MedicalRecordResponse>>> GetMedicalRecordName(string medicalRecordName)
        {
            var query = new GetMedicalRecordQueryByName(medicalRecordName);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Authorize]
        [Route("GetAllMedicalRecords")]
        [ProducesResponseType(typeof(IList<MedicalRecordResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IList<MedicalRecordResponse>>> GetAllMedicalRecords([FromQuery] MedicalRecordSpecificationParameters medicalRecordSpecificationParameters)
        {
            var query = new GetAllMedicalRecordsQuery(medicalRecordSpecificationParameters);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpPost]
        [Route("CreateMedicalRecord")]
        [ProducesResponseType(typeof(MedicalRecordResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MedicalRecordResponse>> CreateMedicalRecord([FromBody] CreateMedicalRecordCommand medicalRecordCommand)
        {
            var result = await _mediator.Send(medicalRecordCommand);
            return Ok(result);
        }

        [HttpPut]
        [Route("UpdateMedicalRecord")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateMedicalRecord([FromBody] UpdateMedicalRecordCommand updateMedicalRecordCommand)
        {
            var result = await _mediator.Send(updateMedicalRecordCommand);
            return Ok(result);
        }

        [HttpDelete]
        [Route("{id}", Name = "DeleteMedicalRecord")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteMedicalRecord(string id)
        {
            var query = new DeleteMedicalRecordByIdQuery(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }
    }
}
