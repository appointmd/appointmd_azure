#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Services/MedicalRecords/MedicalRecords.API/MedicalRecords.API.csproj", "Services/MedicalRecords/MedicalRecords.API/"]
COPY ["Services/MedicalRecords/MedicalRecords.Application/MedicalRecords.Application.csproj", "Services/MedicalRecords/MedicalRecords.Application/"]
COPY ["Services/MedicalRecords/MedicalRecords.Core/MedicalRecords.Core.csproj", "Services/MedicalRecords/MedicalRecords.Core/"]
COPY ["Services/MedicalRecords/MedicalRecords.Infrastructure/MedicalRecords.Infrastructure.csproj", "Services/MedicalRecords/MedicalRecords.Infrastructure/"]
RUN dotnet restore "./Services/MedicalRecords/MedicalRecords.API/MedicalRecords.API.csproj"
COPY . .
WORKDIR "/src/Services/MedicalRecords/MedicalRecords.API"
RUN dotnet build "./MedicalRecords.API.csproj" -c Release -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./MedicalRecords.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENV ASPNETCORE_ENVIRONMENT=Development
ENTRYPOINT ["dotnet", "MedicalRecords.API.dll"]