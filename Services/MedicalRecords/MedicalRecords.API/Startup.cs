﻿using HealthChecks.UI.Client;
using MediatR;
using MedicalRecords.Application.Handlers;
using MedicalRecords.Core.Repositories;
using MedicalRecords.Infrastructure.Data;
using MedicalRecords.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace MedicalRecords.API
{
    public class Startup
    {

        public IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
         
            services.AddControllers();
            services.AddApiVersioning();
            services.AddHealthChecks().AddMongoDb(Configuration["DatabaseSettings:ConnectionString"], "Scheduling Mongodb HealthCheck", HealthStatus.Degraded);
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo { Title = "MedicalRecords.API", Version = "v1" }); });
            // DI
            services.AddAutoMapper(typeof(Startup));
            services.AddMediatR(typeof(CreateMedicalRecordsCommandHandler).GetTypeInfo().Assembly);
            services.AddScoped<IMedicalRecordContext, MedicalRecordContext>();
            services.AddScoped<IMedicalRecordRepository, MedicalRecordRepository>();
            // Auth
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://dev-qq6v6kua7p6ps2q7.us.auth0.com/";
                options.Audience = "MedicalRecords API";
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
           
            if (env.IsDevelopment())
            {
               
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MedicalRecords.API v1"));
            }
          


            app.UseRouting();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
            });
        }
    }
}
