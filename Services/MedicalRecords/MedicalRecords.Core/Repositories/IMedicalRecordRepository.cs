﻿using MedicalRecords.Core.Entities;
using MedicalRecords.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Core.Repositories
{
    public interface IMedicalRecordRepository
    {
        Task<Pagination<MedicalRecord>> GetMedicalRecords(MedicalRecordSpecificationParameters medicalRecordSpecificationParameters);

        Task<IEnumerable<MedicalRecord>> GetMedicalRecordsByName(string name);
        Task<MedicalRecord> GetMedicalRecordById(string id);

        Task<MedicalRecord> CreateMedicalRecord(MedicalRecord medicalRecord);
        Task<bool> UpdateMedicalRecord(MedicalRecord medicalRecord);
        Task<bool> DeleteMedicalRecord(string id);
    }
}
