﻿using MedicalRecords.Core.Entities;
using MedicalRecords.Core.Repositories;
using MedicalRecords.Core.Specifications;
using MedicalRecords.Infrastructure.Data;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Infrastructure.Repositories
{
    public class MedicalRecordRepository : IMedicalRecordRepository
    {
        private readonly IMedicalRecordContext _context;

        public MedicalRecordRepository(IMedicalRecordContext context)
        {
            _context = context;
        }

        public async Task<MedicalRecord> CreateMedicalRecord(MedicalRecord medicalRecord)
        {
            await _context.MedicalRecords.InsertOneAsync(medicalRecord);
            return medicalRecord;
        }

        public async Task<bool> DeleteMedicalRecord(string id)
        {
            FilterDefinition<MedicalRecord> filter = Builders<MedicalRecord>.Filter.Eq(m => m.Id, id);
            DeleteResult deleteResult = await _context
                .MedicalRecords
                .DeleteOneAsync(filter);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }

        public async Task<MedicalRecord> GetMedicalRecordById(string id)
        {
            return await _context.MedicalRecords.Find(m => m.Id == id).FirstOrDefaultAsync();
        }

        public async Task<Pagination<MedicalRecord>> GetMedicalRecords(MedicalRecordSpecificationParameters medicalRecordSpecificationParameters)
        {
            var builder = Builders<MedicalRecord>.Filter;
            var filter = builder.Empty;

            if (!string.IsNullOrEmpty(medicalRecordSpecificationParameters.Sort))
            {
                return new Pagination<MedicalRecord>
                {
                    PageSize = medicalRecordSpecificationParameters.PageSize,
                    PageIndex = medicalRecordSpecificationParameters.PageIndex,
                    Data = await _context.MedicalRecords.Find(filter)
                    .Sort(Builders<MedicalRecord>.Sort.Ascending("Name"))
                    .Skip(medicalRecordSpecificationParameters.PageIndex - 1)
                    .Limit(medicalRecordSpecificationParameters.PageSize)
                    .ToListAsync(),
                    Count = await _context.MedicalRecords.CountDocumentsAsync(p => true) // apply UI
                };
            }

            return new Pagination<MedicalRecord>()
            {
                PageSize = medicalRecordSpecificationParameters.PageSize,
                PageIndex = medicalRecordSpecificationParameters.PageIndex,
                Data = await _context.MedicalRecords
                 .Find(filter)
                 .Sort(Builders<MedicalRecord>.Sort.Ascending("Name"))
                 .Skip(medicalRecordSpecificationParameters.PageSize * (medicalRecordSpecificationParameters.PageIndex - 1))
                 .Limit(medicalRecordSpecificationParameters.PageSize).ToListAsync(),
                Count = await _context.MedicalRecords.CountDocumentsAsync(p => true)
            };
        }

        public async Task<IEnumerable<MedicalRecord>> GetMedicalRecordsByName(string name)
        {
            FilterDefinition<MedicalRecord> filter = Builders<MedicalRecord>.Filter.Eq(m => m.Name, name);
            return await _context
                .MedicalRecords
                .Find(filter)
                .ToListAsync();
        }

        public async Task<bool> UpdateMedicalRecord(MedicalRecord medicalRecord)
        {
            var updateResult = await _context
              .MedicalRecords
              .ReplaceOneAsync(m => m.Id == medicalRecord.Id, medicalRecord);

            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }
    }
}
