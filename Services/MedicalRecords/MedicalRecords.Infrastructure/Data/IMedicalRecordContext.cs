﻿using MedicalRecords.Core.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Infrastructure.Data
{
    public interface IMedicalRecordContext
    {
        IMongoCollection<MedicalRecord>MedicalRecords { get; }
    }
}
