﻿using MedicalRecords.Core.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Infrastructure.Data
{
    public class MedicalRecordContext : IMedicalRecordContext
    {
        public IMongoCollection<MedicalRecord> MedicalRecords { get; }

        public MedicalRecordContext(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
            var database = client.GetDatabase(configuration.GetValue<string>("DatabaseSettings:DatabaseName"));

            MedicalRecords = database.GetCollection<MedicalRecord>(configuration.GetValue<string>("DatabaseSettings:MedicalRecordsCollection"));
          

            MedicalRecordContextSeeder.SeedData(MedicalRecords);
         
        }
    }
}
