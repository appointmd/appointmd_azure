﻿using MedicalRecords.Core.Entities;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalRecords.Infrastructure.Data
{
    public static class MedicalRecordContextSeeder
    {

        public static void SeedData(IMongoCollection<MedicalRecord> medicalRecordCollection)
        {
            bool checkRecords = medicalRecordCollection.Find(m => true).Any();

            string path = Path.Combine("Data", "SeedData", "medicalrecords.json");

            if(!checkRecords)
            {
                var medicalRecordData = File.ReadAllText(path);
                var medicalRecords = JsonConvert.DeserializeObject<List<MedicalRecord>>(medicalRecordData);
                if(medicalRecords != null)
                {
                    foreach(var  medicalRecord in medicalRecords)
                    {
                        medicalRecordCollection.InsertOneAsync(medicalRecord);
                    }
                }
            }
        }
    }
}
