﻿using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Core.Repositories
{
    public interface IAppointmentsRepository
    {
        Task<IEnumerable<Appointment>> getAllAppoinments();

        Task<Appointment> GetAppointmentById(string id);

        Task<Appointment> CreateAppointment(Appointment appointment);
        Task<bool> EditAppointment(Appointment appointment);

        Task<bool> CancelAppointment(string id);
       
        Task<Appointment> SetAppointmentStateFinished(Appointment appointment);
        //TODO: change to set active
        // Appointment should get unactivated after scheduled time has eloped or after medical proffesional change state to done.
        //TODO: change state to set unactive

    }
}
