﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Core.Entities
{
    public class Agenda : BaseEntity
    {

        [BsonElement("Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public User Owner { get; set; }
        public List<Appointment> Appointments { get; set; }
    }
}
