﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Core.Entities
{
    public class User : BaseEntity
    {
        [BsonElement("Name")]
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
       // public string Status { get; set; }
    }
}
