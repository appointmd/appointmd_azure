﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Core.Entities
{
    public class Appointment : BaseEntity
    {
        [BsonElement("Name")]
        public string Name { get; set; }
        public string Status { get; set; }
        public DateTime PlannedForDate { get; set; }

        public string AgendaId { get; set; }

      //  public List<User> Guests { get; set; }
    }
}
