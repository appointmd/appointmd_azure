﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Infrastructure.Data
{
    public class ScheduleContext : IScheduleContext
    {
        public IMongoCollection<Agenda> Agendas { get; }
        public IMongoCollection<Appointment> Appointments { get; }
        public IMongoCollection<User> Users { get; }

        public ScheduleContext(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
            var database = client.GetDatabase(configuration.GetValue<string>("DatabaseSettings:DatabaseName"));

            Agendas = database.GetCollection<Agenda>(configuration.GetValue<string>("DatabaseSettings:AgendasCollection"));
            Appointments = database.GetCollection<Appointment>(configuration.GetValue<string>("DatabaseSettings:AppointmentsCollection"));
            Users = database.GetCollection<User>(configuration.GetValue<string>("DatabaseSettings:UsersCollection"));

            AgendaContextSeeder.SeedData(Agendas);
            AppointmentContextSeeder.SeedData(Appointments);
            UserContextSeeder.SeedData(Users);

            Console.WriteLine(Agendas.EstimatedDocumentCount());
        }
    }
}
