﻿using MongoDB.Driver;
using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Infrastructure.Data
{
    public interface IScheduleContext
    {
        IMongoCollection<Agenda> Agendas { get; }
        IMongoCollection<Appointment> Appointments { get; }
        IMongoCollection<User> Users { get; }
    }
}
