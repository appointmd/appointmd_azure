﻿using MongoDB.Driver;
using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Scheduling.Infrastructure.Data
{
    public static class AppointmentContextSeeder
    {
        public static void SeedData(IMongoCollection<Appointment> AppointmentCollection)
        {
            // check if seeded
            bool checkAppointments = AppointmentCollection.Find(a => true).Any();
            string path = Path.Combine("Data", "SeedData", "appointments.json");
            // if not seeded, check datafile and seed
            if (!checkAppointments)
            {
                var appointmentData = File.ReadAllText(path);
                var appointments = JsonSerializer.Deserialize<List<Appointment>>(appointmentData);
                if (appointments != null)
                {
                    foreach (var appointment in appointments)
                    {
                        AppointmentCollection.InsertOneAsync(appointment);
                    }
                }
            }
        }
    }
}
