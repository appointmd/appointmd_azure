﻿using MediatR;
using Scheduling.Application.Mappers;
using Scheduling.Application.Queries;
using Scheduling.Application.Responses;
using Scheduling.Core.Repositories;
using Scheduling.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class GetAllAgendasQueryHandler : IRequestHandler<GetAllAgendasQuery, Pagination<AgendaResponse>>
    {
        private readonly IAgendasRepository _agendaRepository;

        public GetAllAgendasQueryHandler(IAgendasRepository agendasRepository)
        {
            _agendaRepository = agendasRepository;
        }

        public async Task<Pagination<AgendaResponse>> Handle(GetAllAgendasQuery request, CancellationToken cancellationToken)
        {
            var agendas = await _agendaRepository.GetAgendas(request.SpecificationParameters);
            var agendaResponses = AgendaMapper.Mapper.Map<Pagination<AgendaResponse>>(agendas);
            return agendaResponses;
           
        }
    }
}
