﻿using MediatR;
using Scheduling.Application.Commands;
using Scheduling.Core.Entities;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class UpdateAgendaCommandHandler : IRequestHandler<UpdateAgendaCommand, bool>
    {
        private readonly IAgendasRepository _agendasRepository;

        public UpdateAgendaCommandHandler(IAgendasRepository agendasRepository)
        {
            _agendasRepository = agendasRepository;
        }

        public async Task<bool> Handle(UpdateAgendaCommand request, CancellationToken cancellationToken)
        {
            var agendaEntity = await _agendasRepository.UpdateAgenda(new Agenda
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                Owner = request.Owner,
                Appointments = request.Appointments
            });

            return agendaEntity;
           
        }
    }
}
