﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Scheduling.Application.Commands;
using Scheduling.Core.Repositories;

namespace Scheduling.Application.Handlers
{
    public class DeleteAppointmentByIdCommandHandler : IRequestHandler<DeleteAppointmentByIdCommand, bool>
    {
        private readonly IAppointmentsRepository _appointmentsRepository;

        public DeleteAppointmentByIdCommandHandler(IAppointmentsRepository appointmentsRepository)
        {
            _appointmentsRepository = appointmentsRepository;
        }

        public async Task<bool> Handle(DeleteAppointmentByIdCommand request, CancellationToken cancellationToken)
        {
            return await _appointmentsRepository.CancelAppointment(request.Id);
        }
    }
}
