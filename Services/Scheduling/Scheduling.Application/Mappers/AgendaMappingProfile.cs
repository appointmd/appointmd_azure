﻿using Scheduling.Application.Responses;
using Scheduling.Core.Entities;
using AutoMapper;
using Scheduling.Application.Commands;
using Scheduling.Core.Specifications;
using Scheduling.Core.Events;
using EventBus.Messages.Events;
using Scheduling.Application.Queries;

namespace Scheduling.Application.Mappers
{
    public class AgendaMappingProfile : Profile
    {
        public AgendaMappingProfile()
        {
            CreateMap<Agenda, AgendaResponse>().ReverseMap();
            CreateMap<Agenda, CreateAgendaCommand>().ReverseMap();
            CreateMap<Appointment, AppointmentResponse>().ReverseMap();
            CreateMap<Appointment, CreateAppointmentCommand>().ReverseMap();
            CreateMap<User, UserResponse>().ReverseMap();
            CreateMap<Pagination<Agenda>, Pagination<AgendaResponse>>().ReverseMap();
            CreateMap<RealizeAppointment, FinishAppointmentCommand>().ReverseMap();
            CreateMap<RealizeAppointment, RealizeAppointmentEvent>().ReverseMap();
        }
       
    }
}
