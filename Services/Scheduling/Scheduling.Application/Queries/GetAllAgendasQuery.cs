﻿using Amazon.Runtime.Internal;
using MediatR;
using Scheduling.Application.Responses;
using Scheduling.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Queries
{
    public class GetAllAgendasQuery : IRequest<Pagination<AgendaResponse>>
    {
        public SchedulingSpecificationParameters SpecificationParameters { get; set; }

        public GetAllAgendasQuery(SchedulingSpecificationParameters schedulingSpecificationParameters)
        {
            SpecificationParameters = schedulingSpecificationParameters;
        }
    }
}
