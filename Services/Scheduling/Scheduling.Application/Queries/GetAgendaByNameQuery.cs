﻿using MediatR;
using Scheduling.Application.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Queries
{
    public class GetAgendaByNameQuery : IRequest<IList<AgendaResponse>>
    {
        public string Name { get; set; }

        public GetAgendaByNameQuery(string name)
        {
            Name = name;
        }
    }
}
