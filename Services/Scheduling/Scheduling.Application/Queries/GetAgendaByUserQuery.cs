﻿using Amazon.Runtime.Internal;
using MediatR;
using Scheduling.Application.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Queries
{
    public class GetAgendaByUserQuery : IRequest<IList<AgendaResponse>>
    {
        public string owner { get; set; }

        public GetAgendaByUserQuery(string ownername)
        {
            owner = ownername;
        }
    }
}
