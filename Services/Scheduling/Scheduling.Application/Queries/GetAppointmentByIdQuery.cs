﻿using Scheduling.Application.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace Scheduling.Application.Queries
{
    public class GetAppointmentByIdQuery : IRequest<AppointmentResponse>
    {
        public string Id { get; set; }

        public GetAppointmentByIdQuery(string id)
        {
            Id = id;
        }
    }
}
