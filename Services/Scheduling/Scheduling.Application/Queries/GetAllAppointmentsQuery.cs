﻿using Amazon.Runtime.Internal;
using MediatR;
using Scheduling.Application.Responses;
using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Queries
{
    public class GetAllAppointmentsQuery : IRequest<IList<AppointmentResponse>>
    {
    }
}
