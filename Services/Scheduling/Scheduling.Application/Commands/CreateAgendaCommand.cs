﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Runtime.Internal;
using Scheduling.Application.Responses;
using MediatR;

namespace Scheduling.Application.Commands
{
    public class CreateAgendaCommand : IRequest<AgendaResponse>
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public User Owner { get; set; }
        public Appointment Appointments { get; set; }
    }
}
