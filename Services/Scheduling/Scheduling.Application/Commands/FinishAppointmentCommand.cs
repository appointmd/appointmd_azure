﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using Scheduling.Application.Responses;

namespace Scheduling.Application.Commands
{
    public class FinishAppointmentCommand : IRequest<AppointmentResponse>
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }
        public string Status { get; set; }
        public DateTime PlannedForDate { get; set; }
        public string AgendaId { get; set; }

    }
}
