﻿using HealthChecks.UI.Client;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.OpenApi.Models;
using Scheduling.Application.Handlers;
using Scheduling.Core.Repositories;
using Scheduling.Infrastructure.Data;
using Scheduling.Infrastructure.Repositories;
using System.Reflection;

namespace Scheduling.API
{
    public class Startup
    {
        public IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services) 
        {
           
            services.AddControllers();
            services.AddApiVersioning();
            services.AddHealthChecks().AddMongoDb(Configuration["DatabaseSettings:ConnectionString"], "Scheduling Mongodb HealthCheck", HealthStatus.Degraded);
            
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo { Title = "Scheduling.API", Version = "v1" }); });
            // DI
            services.AddAutoMapper(typeof(Startup));
            services.AddMediatR(typeof(CreateAgendaCommandHandler).GetTypeInfo().Assembly);
            services.AddScoped<IScheduleContext, ScheduleContext>();
            services.AddScoped<IAgendasRepository, SchedulingRepository>();
            services.AddScoped<IAppointmentsRepository, SchedulingRepository>();
            services.AddScoped<IUsersRepository, SchedulingRepository>();

            services.AddMassTransit(config =>
            {
                config.UsingRabbitMq((ct, cfg) =>
                {
                    cfg.Host(Configuration["EventBusSettings:HostAdress"]);
                });
            });
            services.AddMassTransitHostedService();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://dev-qq6v6kua7p6ps2q7.us.auth0.com/";
                options.Audience = "http://localhost:9000/api/v1/Scheduling";
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ReadAccess", policy =>
                                  policy.RequireClaim("permissions", "read:appointments"));
                options.AddPolicy("WriteAccess", policy =>
                                  policy.RequireClaim("permissions", "write:appointments"));
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            if (env.IsDevelopment())
            {
                
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Scheduling.API v1"));
            }
        
           

            app.UseRouting();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
            });
        }
    }
}
