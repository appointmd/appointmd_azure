﻿using EventBus.Messages.Events;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web.Resource;
using Scheduling.Application.Commands;
using Scheduling.Application.Mappers;
using Scheduling.Application.Queries;
using Scheduling.Application.Responses;
using Scheduling.Core.Entities;
using Scheduling.Core.Events;
using Scheduling.Core.Specifications;
using System.Net;


namespace Scheduling.API.Controllers
{
    public class SchedulingController : ApiController
    {
        private readonly IMediator _mediator;

        private readonly IPublishEndpoint _publishEndpoint;

        public SchedulingController(IMediator mediator, IPublishEndpoint publishEndpoint)
        {
            _mediator = mediator;
            _publishEndpoint = publishEndpoint;
        }

        [HttpGet]
        [Route("[action]/{id}", Name = "GetAgendaById")]
        [ProducesResponseType(typeof(AgendaResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<AgendaResponse>> GetAgendaById(string id)
        {
            var query = new GetAgendaByIdQuery(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("appointments/[action]/{id}", Name = "GetAppointmentById")]
        [ProducesResponseType(typeof(AppointmentResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<AppointmentResponse>> GetAppointmentById(string id)
        {
            var query = new GetAppointmentByIdQuery(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("[action]/{agendaName}", Name = "GetAgendaByName")]
        [ProducesResponseType(typeof(IList<AgendaResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IList<AgendaResponse>>> GetAgendaByName(string agendaName)
        {
            var query = new GetAgendaByNameQuery(agendaName);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllAgendas")]
        [Authorize(Policy = "ReadAccess")]
        [ProducesResponseType(typeof(IList<AgendaResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IList<AgendaResponse>>> GetAllAgendas([FromQuery] SchedulingSpecificationParameters schedulingSpecificationParameters)
        {
            var query = new GetAllAgendasQuery(schedulingSpecificationParameters);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllAppointments")]
        [Authorize(Policy = "WriteAccess")]
        [ProducesResponseType(typeof(IList<AppointmentResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IList<AppointmentResponse>>> GetAllAppointments()
        {
            var query = new GetAllAppointmentsQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllUsers")]
        [ProducesResponseType(typeof(IList<UserResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IList<UserResponse>>> GetAllUsers()
        {
            var query = new GetAllUsersQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("[action]/{user}", Name = "GetAgendaByUserName")]
        [ProducesResponseType(typeof(AgendaResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<AgendaResponse>> GetAgendaByUserName(string user)
        {
            var query = new GetAgendaByUserQuery(user);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // POST
        [HttpPost]
        [Route("CreateAgenda")]
        [ProducesResponseType(typeof(AgendaResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<AgendaResponse>> CreateAgenda([FromBody] CreateAgendaCommand agendaCommand)
        {
            var result = await _mediator.Send(agendaCommand);
            return Ok(result);
        }

        [HttpPost]
        [Route("CreateAppointment")]
        [ProducesResponseType(typeof(AppointmentResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<AppointmentResponse>> CreateAppointment([FromBody] CreateAppointmentCommand appointmentCommand)
        {
            var result = await _mediator.Send(appointmentCommand);
            return Ok(result);
        }

        // UPDATE
        [HttpPut]
        [Route("UpdateAgenda")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateAgenda([FromBody] UpdateAgendaCommand agendaCommand)
        {
            var result = await _mediator.Send(agendaCommand);
            return Ok(result);
        }

        [HttpPut]
        [Route("EditAppointment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> EditAppointment([FromBody] EditAppointmentCommand appointmentCommand)
        {
            var result = await _mediator.Send(appointmentCommand);
            return Ok(result);
        }

        // Finish appointment

        [HttpPut]
        [Route("SetAppointmentFinished")]
        [ProducesResponseType(typeof(AppointmentResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(AppointmentResponse), (int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<AppointmentResponse>> SetAppointmentFinished([FromBody] RealizeAppointment appointment)
        {
            
          
            // realizing the appointment using appointment details
            // NOTE: I think technically I should get rid of the query and replace it with event etc etc.
            // set fields that are not included in map -> appointmentID becomes command.id
         

            // get from ID
            var idexistsresult = await _mediator.Send(new GetAppointmentByIdQuery(appointment.Id));

            // Check if ID is valid
            if (idexistsresult == null)
            {
                return NotFound();
            }
            
            // map to finish command and execute finish command so values change
            // Finish appointment
            // var result = await _mediator.Send(appointment);
           
            var eventMessage = AgendaMapper.Mapper.Map<RealizeAppointmentEvent>(appointment);
            eventMessage.AppointmentId = appointment.Id;

            await _publishEndpoint.Publish(eventMessage);

            // return OK
            return Ok(eventMessage);
        }


        // DELETE
        [HttpDelete]
        [Route("{id}", Name = "DeleteAgenda")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteAgenda(string id)
        {
            var command = new DeleteAgendaByIdCommand(id);
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [Route("appointment/{appointmentid}", Name = "DeleteAppointment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteAppointment(string appointmentid)
        {
            var command = new DeleteAppointmentByIdCommand(appointmentid);
            var result = await _mediator.Send(command);
            return Ok(result);

        }
    }
}
