﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBus.Messages.Events
{
    public class BaseIntegrationEvent
    {
        public Guid guiId { get; private set; }
        public DateTime CreationDate { get; private set; }

        public BaseIntegrationEvent()
        {
            guiId = Guid.NewGuid();
            CreationDate = DateTime.Now;
        }

        public BaseIntegrationEvent(Guid id, DateTime creationDate)
        {
            guiId = id;
            CreationDate = creationDate;
        }
    }
}
