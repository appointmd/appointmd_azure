﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBus.Messages.Events
{
    public class RealizeAppointmentEvent : BaseIntegrationEvent
    {
        public string? AppointmentId { get; set; }
        public string? Name { get; set; }
        public string? Status { get; set; }
        public DateTime? PlannedForDate { get; set; }
        public string? HostId { get; set; }
    }
}
